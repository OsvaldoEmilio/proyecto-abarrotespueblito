create DATABASE abarrotespueblito;
use abarrotespueblito;

drop table proveedor;
create TABLE proveedor(
idproveedor int primary KEY AUTO_INCREMENT,
nombre varchar(100),
direccion varchar(100),
telefono varchar(11));

create table cateoria(
idcategoria int primary key AUTO_INCREMENT,
nombrecategoria varchar(100));

create table producto(
idproducto int primary key AUTO_INCREMENT,
nombreproducto varchar(100),
precio double,
fkcategoria int,
foreign KEY(fkcategoria) REFERENCES cateoria(idcategoria));

drop table compras;
create table compras(
idcompras int primary KEY AUTO_INCREMENT,
fkproveedor int,
fkproducto int,
fechacompra date,
cantidad double,
foreign KEY(fkproveedor) REFERENCES proveedor(idproveedor),
foreign KEY(fkproducto) REFERENCES producto(idproducto));

insert into cateoria values(null,'pan'),(null,'bebidas');
insert into producto values(null,'mantecadas',10.50,1),(null,'pan bimbo',35,1);
insert into producto values(null,'cocacola',12,2),(null,'agua bonafont',10,2);
insert into proveedor values(null,'bimbo','juarez #33','7405348'),(null,'cocacola','allende #33','74065482');
insert into compras values(null,1,1,'2020-02-12',10),(null,2,3,'2020-02-12',10);

create view v_compras as
select nombre,nombreproducto,precio,cantidad,fechacompra from compras,producto,proveedor where compras.fkproducto = producto.idproducto and compras.fkproveedor = proveedor.idproveedor;

select * from v_compras;

create PROCEDURE provedorcantidad(
in fkprov int,
in fkprod int,
in fech date,
in cant double)
begin 
if cant >= 1 then
insert into compras values(null,fkprov,fkprod,fech,cant);
 else
select "puto el que lo lea";

end if;
end;

call provedorcantidad(1,1,'2019-04-13',0);
select * from compras;

select * from 