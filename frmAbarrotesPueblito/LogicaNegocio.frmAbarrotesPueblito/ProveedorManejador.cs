﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.AbarrotesPueblito;
using AccesoDatos.frmAbarrotesPueblito;

namespace LogicaNegocio.frmAbarrotesPueblito
{
    public class ProveedorManejador
    {
        private AccesoDatosProveedor _accesoDatosProveedor = new AccesoDatosProveedor();
        public void guardar(Proveedor proveedor)
        {
            _accesoDatosProveedor.guardar(proveedor);
        }
        public void eliminar(int idproveedor)
        {
            _accesoDatosProveedor.eliminar(idproveedor);
        }
        public List<Proveedor> getProveedor(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listproveedor = _accesoDatosProveedor.getproveedor(filtro);


            return listproveedor;

        }
    }
}
