﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.AbarrotesPueblito;
using LogicaNegocio.frmAbarrotesPueblito;

namespace frmAbarrotesPueblito
{
    public partial class Form1 : Form
    {
        private ProveedorManejador _proveedorManejador;
        private Proveedor _proveedor;
        public Form1()
        {
            InitializeComponent();
            _proveedorManejador = new ProveedorManejador();
            _proveedor = new Proveedor();
        }
        private void limpiarcuadros()
        {
            txtNombre.Text = "";
            txtDireccion.Clear();
            txtTelofono.Text = "";           
            lblID.Text = "0";
        }
        private void controlarcuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtTelofono.Enabled = activar;
            
        }
        private void controlarbotones(bool nuevo, bool guardar, bool cancelar, bool eliminar)
        {
            btnNuevo.Enabled = nuevo;
            btnGuardar.Enabled = guardar;
            btnCancelar.Enabled = cancelar;
            btnEliminar.Enabled = eliminar;
        }

        private void buscarproveedor(string filtro)
        {
            dtgProveedor.DataSource = _proveedorManejador.getProveedor(filtro);

        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            controlarbotones(false, true, true, false);
            controlarcuadros(true);
            txtNombre.Focus();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            buscarproveedor("");
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }
        
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            try//control+ks try
            {
                guardarprov();
                limpiarcuadros();
                buscarproveedor("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);//mbox 2tab
            }
        }
        private void guardarprov()
        {
            _proveedorManejador.guardar(new Proveedor
            {
                IdProveedor = Convert.ToInt32(lblID.Text),
                Nombre = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Telefono = txtTelofono.Text,
                

            });
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            controlarbotones(true, false, false, true);
            controlarcuadros(false);
            limpiarcuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta seguro que desea eliminar el registro", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    eliminarprov();
                    buscarproveedor("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void eliminarprov()
        {
            var idproveedor = dtgProveedor.CurrentRow.Cells["idproveedor"].Value;
            _proveedorManejador.eliminar(Convert.ToInt32(idproveedor));
        }

        private void dtgProveedor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Modificarprov();
                buscarproveedor("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void Modificarprov()
        {
            controlarcuadros(true);
            controlarbotones(false, true, true, false);
            lblID.Text = dtgProveedor.CurrentRow.Cells["idproveedor"].Value.ToString();
            txtNombre.Text = dtgProveedor.CurrentRow.Cells["nombre"].Value.ToString();
            txtDireccion.Text = dtgProveedor.CurrentRow.Cells["direccion"].Value.ToString();
            txtTelofono.Text = dtgProveedor.CurrentRow.Cells["telefono"].Value.ToString();
            
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarproveedor(txtBuscar.Text);
        }
    }
}
