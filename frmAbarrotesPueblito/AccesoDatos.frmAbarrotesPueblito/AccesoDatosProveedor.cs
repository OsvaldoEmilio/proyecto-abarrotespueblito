﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.AbarrotesPueblito;
using libreriaconexionmysql;
using System.Data;

namespace AccesoDatos.frmAbarrotesPueblito
{
    public class AccesoDatosProveedor
    {
        conexionAccesoDatos conexion;

        public AccesoDatosProveedor()
        {
            conexion = new conexionAccesoDatos("localhost", "root", "", "abarrotespueblito", 3306);
        }

        public void guardar(Proveedor proveedor)
        {
            if (proveedor.IdProveedor == 0)
            {
                string consulta = string.Format("Insert into proveedor values(null,'{0}','{1}','{2}')", proveedor.Nombre, proveedor.Direccion, proveedor.Telefono);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                string consulta = string.Format("update proveedor set nombre = '{0}', direccion = '{1}',telefono = '{2}' where idproveedor = {3}", proveedor.Nombre,proveedor.Direccion,proveedor.Telefono,proveedor.IdProveedor);
                conexion.Ejecutarconsulta(consulta);
            }
        }
        public void eliminar(int idproveedor)
        {
            string consulta = string.Format("delete from proveedor where idproveedor = {0}", idproveedor);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Proveedor> getproveedor(string filtro)
        {
            var listProveedor = new List<Proveedor>();
            var ds = new DataSet();
            string consulta = "select * from proveedor where nombre like '%" + filtro + "%'";
            ds = conexion.Obtenerdatos(consulta, "proveedor");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach(DataRow Row in dt.Rows)
            {
                var proveedor = new Proveedor
                {
                    IdProveedor = Convert.ToInt32(Row["idproveedor"]),
                    Nombre = Row["nombre"].ToString(),
                    Direccion = Row["direccion"].ToString(),
                    Telefono = Row["telefono"].ToString(),

                };
                listProveedor.Add(proveedor);
            }
            return listProveedor;
        }
    }
}
